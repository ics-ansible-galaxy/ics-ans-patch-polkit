import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_default(host):
    cmd = host.run("/tmp/cve-2021-4034--2022-01-25-0936.sh")
    assert cmd.exit_status
