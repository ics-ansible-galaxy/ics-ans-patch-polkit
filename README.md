# ics-ans-patch-polkit

Ansible playbook to patch polkit - CVE-2021-4034

Variables:
```yaml
do_polkit_update:  # to trigger or not the actual package update
```

## License

BSD 2-clause
